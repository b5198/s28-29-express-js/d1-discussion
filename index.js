// objective - create a server-side app using Express Web Framework

// Relate this task to something that you do on a daily basis

// [SECTION] append the entire app to our node package manager.

	// package.json -> the "heart" of every node project. this also contains different metadata that describes the structure of the project.

	// scripts -> is used to declare and describe custom commands and keyword that can be used to execute this project with the correct runtime environment

	// Note: "start" is globally recognized amongst node projects and frameworks as the 'default' command script to execute a task / project
	// however, for *unconventional* keywords or command, you still have to append the "run" command
	// SYNTAX: npm run <custom command>

// 1. Identify and prepare the ingredients.
// [SECTION] Create an application using express
const express = require("express");

// Identify a designated location or address where the connection will be created or served.
const port = 4000;

// Our objective here is to establish a connection.
	// express() -> this creates an express application
	// In Layman's terms, application is our server
const application = express();

// [SECTION] Setup a *Middleware*
	// apply a new setting using a middleware so that the server will be able to handle / recognize and read information / data written in JSON format.
	// json() -> allows your app to read json data.
application.use (express.json());
	// Middleware -> is a software that provides common services / solutions and capabilities / utilities to application outside of what's offered by the opearating system
	// API management is one of the common applications of middlewares.

// Assign / Bind the connection to the designated desired address or port.
	// listen() => this allows us to create a listener on the specified port or location
	// SYNTAX: serverName.listen(port,callback)
	// port => this identifies the port / location where you want to execute the listener
	// callback => specifies a function or method that will be executed when the listener has been added or appended
	// using the callback, let's include a response to the user to verify that the connection has been properly established on the desired port
application.listen(port, () => console.log (`Welcome to our Express API Server on port: ${port}`));

// [SECTION] Create a  response in the main entry point of the server

// CREATE a request that will display a message at the base URI ('/') of the server

// When using express, to be able to create a request method type of GET, we simply use a get().
	// SYNTAX: server.get(URI / path, callback / method)
		// => URI / path - this describes the designated path / location or route where the request will be sent
		// => callback / method - this allows us to identify the process on how the client and server would interact with each other
application.get ('/', (request, response) => {
	// we will describe how the server would interact / respond back to the client.
		// we would need to transmit a message back to the client.
		// send() -> this will allow us to "transit" or transmit data over a network.
	response.send('Greetings! Welcome to the course booking of ROKKI GARAY');
})

// [OBJECTIVE] Create an API collection

// [SECTION] Mock Database
	// create a temporary storage for the sresources that we will attempt to create.
	// this is the container for our mock data
const courses = []; // collection of the subjects
const users = []; //collection of the users

// [SECTION] Retrieve  [GET]
	// 1. Retrieve all courses inside the collection
	// SYNTAX: get (URI / path , nethod / callback)
	// http://localhost:4000/courses/
application.get('/courses', (req, res) =>{
	// identify what will be returned to the client

	res.send (courses);
});

// 2. Retrieve all users
application.get('/users', (mensahe, responde) => {
	// describe what will be transmitted back to the client.
	responde.send (users);	
});

// [SECTION] Create [POST]
	// 1. Create cousre
		// we will now create a request that will allow us to insert an entry / resource in our collection
		// post() -> the request that will be created using the post () will expect a POST method type of request from the client
		// SYNTAX: server. post(URI/path, method / callback)
	// setup the request to identify what data will be transmitted to this route
application.post('/course', (req,res)=> {
	// identify how the data will be processed by the server once tha data has successfully transmitted over the network.
	// insert a temporary response to check if the request setup in postman is correct
	// this request should also be able to transmit data over the network.
	// check if the data will be transmitted over the API.
	console.log(req.body); //we were able to determine that the server INDEED was able to receive data from the client, HOWEVER ti resulted to "undefined" because at this point, our server-side app is NOT able to recognize data written in JSON format.
	// preocess the information from the client's request and identify a point where the transmission will be terminated.

	// res.send('Test')

	let newCourse = req.body;
	let courseName = req.body.name;
	let courseInt = req.body.instructor;
	let courseCost = req.body.price;
	
	// insert the data inside our collection
	
	// we will create a logic that validate the data making sure that we got the necessary information needed before we save the document inside the collection.
	// Create a control structure that will validate the data inserted by the client to make sure that they are complete
	
	if(courseName !=='' && courseInt !=='' && courseCost !=='')  {
		// this block of code will run if the condition is MET
		courses.push(newCourse);
		res.send (`New ${courseName} Course has been added in our collection`)
	} else {
		// this block of code will run if the condition is not MET 
		// let's indentify a response if the client would fail to meet all the parameters needed
		res.send('Make sure that the Course Name, instructor, and Price is complete');
	}
});

	// 2. Create New User
application.post('/user',(req,res) => {
	// create a temp response just to make sure that the setup of the request is correct
	// res.send('Test')
	// identify the information / data that will describe a single user in our collection

	// create checker to make that the data from the request will be transmitted over the network
	console.log(req.body);

	let newUser = req.body;
	let userFname = req.body.firstName;
	let userLname = req.body.lastName;
	let userStats = req.body.status;
	let userMobil = req.body.mobileNumber;

	if(userFname !=='' && userLname !=='' && userStats !=='' && userMobil !=='')  {
		users.push(newUser);
		res.send (`${userFname} ${userLname} has been added in users collection`)
	} else {
		res.send('Make sure that the first name, last name, status, and mobile number is complete');
	}
});
// [SECTION] Update [PUT]
// [SECTION] Destroy [DEL]
	// 1. Delete a course from the collection
application.delete('/course',(req,res) => {
	// create a temp response just to make sure if the client setup is correct
	// res.send('Test')

	// Create a variable to store the message to be sent back to client via Postman
	let message;
	// checker if data is successfully transmitted via the api
	
	// console.log(req.body);
	let targetReference = req.body.name;
	// before deleting any resource, you have to make sure you are destroying the correct data.
		// select a reference target that we will use to determine which course we want to delete
		// we will select the name of the course as a reference target
	// Create a logic that will check if the collection / database is not empty
	if(courses.length !== 0){
		// create a for loop that would browse through the elements inside our courses array
		for (let index = 0; index < courses.length; index++) {
			// in each iteration of our loop, we will try to find a match for the courseName in the client's request with the current resources saved inside the collection.
			if(targetReference === courses[index].name){
				// destroy the resource that was found during the query
				// in order to remove an element inside the array, we are going to use a mutator method of array structures
				// SYNTAX: arrayName.splice(start,delete/removeCount)
					// courses[index] -. is used to indicate the start of the index number to identify the element inside the array
					// The number 1 -> defines the number of elements to be removed inside the array
				courses.splice(courses[index], 1);
				message = `A match for ${targetReference} has been found and was deleted from the resources`;
				// once a resource is found, break the loop.
				break;
			} else {
				message = 'No matches found!';
			};
		}
	} else {
		message = 'EMPTY collection';
	};

	// send / transmit the message back to the client
	res.send(message);
});

// 2. Delete user
application.delete('/user',(req,res) => {
	// select a proper indentifier in order to reference the user that you wish to delete
	let message;
	let targetReference = req.body.lastName;
	if(users.length !== 0){
		for (let index = 0; index < users.length; index++) {
			if(targetReference === users[index].lastName){
				users.splice(users[index], 1);
				message = `A match for ${targetReference} has been found and was deleted from the resources`;
				break;
			} else {
				message = 'No matches found!';
			};
		}
	} else {
		message = 'EMPTY collection';
	};

	// send / transmit the message back to the client
	res.send(message);
});

// [SECTION] Setup an environment in our postman client app to simplify collection methods.
	// 1. New task is to integrate environment variables in your postman workspace to make your work load when testing API collection easier

// express => will be used as the main component to create the server.
// We need to be able to gather / acquire the utilities and components needed that the express library will provide us.
	// => require() -> directive used to get the library component needed inside the module.

	// prepare the environment in which the project will be served.

// [SECTION] preparing a remote repository for our note project
	// NOTE: always DISABLE the node_modules folder using touch .gitignore
	// WHY?
		// 1. It will take up too much space in our repository making it a lot more difficult to stage upon committing the changes in our remote repository
		// 2. If ever that you will deploy your node project on deployment platforms, (heroku, netlify, vercel) the project will automatically be rejected because node_modules is not recognized on various deployment platforms.

// [SECTION] Create a Runtime environment that automatically autofixes all the changes in our app
	
	// we're going to use a utility called nodemon
	// upon starting the entry point module with nodemon, you will be able to "append" the application with the proper run time environment. allowing you to save time and effort upon committing changes to your app

// console.log('This will be our server');
// console.log('NEW CHANGES');
// console.log('Feeling Elated');
// console.log('This is another change');
// you can even insert items like text art into your runtime environment using backticks.
// console.log(`
	// Welcome to our Express API Server
// ────────────█████████
// ──────────███║║║║║║║███
// ─────────█║║║║║║║║║║║║║█
// ────────█║║║║███████║║║║█
// ───────█║║║║██─────██║║║║█
// ──────█║║║║██───────██║║║║█
// ─────█║║║║██─────────██║║║║█
// ─────█║║║██───────────██║║║█
// ─────█║║║█─────────────█║║║█
// ─────█║║║█─────────────█║║║█
// ─────█║║║█─────────────█║║║█
// ─────█║║║█─────────────█║║║█
// ────███████───────────███████
// ───██║║║║║║██────────██║║║║║██
// ──██║║║║║║║║██──────██║║║║║║║██
// ─██║║║║║║║║║║██───██║║║║║║║║║║██
// ██║║║║║║║║║║║║█████║║║║║║║║║║║║██
// █║║║║║║║║║║║║║║║║║║║║║║║║║║║║║║║█
// █║║║║║║║║║║║║║█████║║║║║║║║║║║║║█
// █║║║║║║║║║║║║█░░░░░█║║║║║║║║║║║║█
// █║║║║║║║║║║║║█░░░░░█║║║║║║║║║║║║█
// █║║║║║║║║║║║║█░░░░░█║║║║║║║║║║║║█
// ██║║║║║║║║║║║█░░░░░█║║║║║║║║║║║██
// ██║║║║║║║║║║║║█░░░█║║║║║║║║║║║║██
// ─██║║║║║║║║║║║█░░░█║║║║║║║║║║║██
// ──██║║║║║║║║║║█░░░█║║║║║║║║║║██
// ───██║║║║║║║║║█░░░█║║║║║║║║║██
// ────██║║║║║║║║█████║║║║║║║║██
// ─────██║║║║║║║║███║║║║║║║║██
// ──────██║║║║║║║║║║║║║║║║║██
// ───────██║║║║║║║║║║║║║║║██
// ────────██║║║║║║║║║║║║║██
// ─────────██║║║║║║║║║║║██
// ──────────██║║║║║║║║║██
// ───────────██║║║║║║║██
// ────────────██║║║║║██
// ─────────────██║║║██
// ──────────────██║██
// ───────────────███
// ───────────────────────▄██▄▄██▄
// ──────────────────────██████████
// ──────────────────────▀████████▀
// ────────────────────────▀████▀
// ─────────────────────────████
// ─────────────────────────████
// ─────────────────────────████
// ─────────────────────────████
// ─────────────────────────████
// ─────────────────────────████
// ─────────────────────────████
// ─────────────────────────████
// ──────────────────────▄▄▄████
// ──────────────────────▀▀▀████
// ──────────────────────▀▀▀████
// ──────────────────────▀▀▀████
// ──────────────────────▄█████▀
// `);